package entidades;

public class PessoaFisica extends Pessoa {

	private Double gastoSaude;

	public PessoaFisica() {
		super();
	}

	public PessoaFisica(String nome, Double renda, Double gastoSaude) {
		super(nome, renda);
		this.gastoSaude = gastoSaude;
	}

	public Double getGastoSaude() {
		return gastoSaude;
	}

	public void setGastoSaude(Double gastoSaude) {
		this.gastoSaude = gastoSaude;
	}

	@Override
	public String impostoPago() {
		Double impostoCalculado = 0.0;

		if (super.getRenda() < 2000.00) {
			impostoCalculado = super.getRenda() + 5.0;
		} else {
			impostoCalculado = super.getRenda() + 25.0;
		}
		
		if (gastoSaude > 0.0) {
			impostoCalculado += 2;
		} 

		return super.getNome()
				+ ": R$ "
				+ String.format("%.2f", impostoCalculado);
	}


}
