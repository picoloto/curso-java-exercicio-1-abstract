package entidades;

public class PessoaJuridica extends Pessoa {

	private Integer numeroFuncionarios;

	public PessoaJuridica() {
		super();
	}

	public PessoaJuridica(String nome, Double renda, Integer numeroFuncionarios) {
		super(nome, renda);
		this.numeroFuncionarios = numeroFuncionarios;
	}


	public Integer getNumeroFuncionarios() {
		return numeroFuncionarios;
	}

	public void setNumeroFuncionarios(Integer numeroFuncionarios) {
		this.numeroFuncionarios = numeroFuncionarios;
	}

	@Override
	public String impostoPago() {
		Double impostoCalculado = 0.0;

		if (super.getRenda() < 2000.00) {
			impostoCalculado = super.getRenda() + 10.0;
		} else {
			impostoCalculado = super.getRenda() + 35.0;
		}
		
		if (numeroFuncionarios > 0.0) {
			impostoCalculado += 5;
		} 

		return super.getNome()
				+ ": R$ "
				+ String.format("%.2f", impostoCalculado);
	}

}
