package aplicacao;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entidades.Pessoa;
import entidades.PessoaFisica;
import entidades.PessoaJuridica;

public class Programa {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Pessoa> pessoas = new ArrayList<>();
		
		System.out.print("Quantas pessoas deseja Cadastrar? ");
		int n = sc.nextInt();
		sc.nextLine();
		
		System.out.println();
		for (int i = 1; i <= n; i++) {
			System.out.println("Pessoa #"+ i);
			System.out.print("Fisica ou Juridica (f/j): ");
			char t = sc.next().charAt(0);
			sc.nextLine();
			System.out.print("Nome: ");
			String nome = sc.nextLine();
			System.out.print("Renda: ");
			double renda = sc.nextDouble();
			
			if (t == 'f') {
				System.out.print("Gasto com Sa�de: ");
				double gastoSaude = sc.nextDouble();
				
				pessoas.add(new PessoaFisica(nome, renda, gastoSaude));
			} else {
				System.out.print("N�mero de Funcionarios: ");
				int numeroFuncionarios = sc.nextInt();
				
				pessoas.add(new PessoaJuridica(nome, renda, numeroFuncionarios));
			}
			
			System.out.println();
		}
		
		System.out.println("Taxas: ");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa.impostoPago());
		}
		
		sc.close();
	}

}
